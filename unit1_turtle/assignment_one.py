import turtle

turtle.speed(2)

def drawOctagon(octagoncolor):
    turtle.color(octagoncolor)
    turtle.begin_fill()
    for y in range(8):
        turtle.forward(50)
        turtle.right(45)
    turtle.end_fill()


def goTo(xcord, ycord):
    turtle.penup()
    turtle.goto(xcord, ycord)
    turtle.pendown()


drawOctagon("red")
goTo(150, 0)
drawOctagon("orange")
goTo(300, 0)
drawOctagon("green")
goTo(150, 150)
drawOctagon("grey")

turtle.done()
