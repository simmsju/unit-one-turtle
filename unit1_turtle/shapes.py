import turtle

turtle.circle(50)

turtle.penup()
turtle.forward(200)
turtle.pendown()

for x in range(4):
    turtle.forward(100)
    turtle.left(90)

turtle.left(90)
turtle.penup()
turtle.forward(200)
turtle.pendown()

for w in range(4):
    turtle.forward(100)
    turtle.right(120)