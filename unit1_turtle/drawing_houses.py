import turtle

def drawAHouse(size, houseColor, roofColor):
    turtle.begin_fill()
    for x in range(4):
        turtle.color(houseColor)
        turtle.forward(size)
        turtle.right(90)
    turtle.end_fill()
    turtle.begin_fill()
    for y in range(3):
        turtle.color(roofColor)
        turtle.forward(size)
        turtle.left(120)
    turtle.end_fill()
    turtle.forward(size)


drawAHouse(50, "green", "orange")
turtle.penup()
turtle.forward(70)
turtle.pendown()
drawAHouse(30, "red", "purple")
turtle.penup()
turtle.forward(70)
turtle.pendown()
drawAHouse(70, "grey", "black")
turtle.penup()
turtle.forward(70)
turtle.pendown()
drawAHouse(10, "yellow", "blue")

turtle.done()
